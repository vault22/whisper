# Whisper
Whisper is an encryption program written in Bash that makes it easy to encrypt and decrypt new or existing files. It uses Gem as a dependency for visualization, making the user experience more intuitive and efficient. With Whisper, you can easily secure new or existing files with just a few keyboard presses - perfect for those who need reliable data security without any hassle!

![Example Animation](./example.gif)

## Installing Gum Dependency

This program requires Gum to work properly. To install Gum, follow these instructions:

Use a package manager:

```
bash
# macOS or Linux
brew install gum

# Arch Linux (btw)
pacman -S gum

# Nix
nix-env -iA nixpkgs.gum
# Or, with flakes
nix run "github:charmbracelet/gum" -- --help

# Debian/Ubuntu

sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://repo.charm.sh/apt/gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/charm.gpg
echo "deb [signed-by=/etc/apt/keyrings/charm.gpg] https://repo.charm.sh/apt/ * *" | sudo tee /etc/apt/sources.list.d/charm.list
sudo apt update && sudo apt install gum


# Fedora/RHEL

echo '[charm]
name=Charm
baseurl=https://repo.charm.sh/yum/
enabled=1
gpgcheck=1
gpgkey=https://repo.charm.sh/yum/gpg.key' | sudo tee /etc/yum.repos.d/charm.repo
sudo yum install gum


# Alpine
apk add gum

# Android (via termux)
pkg install gum

# Windows (via WinGet or Scoop)
winget install charmbracelet.gum
scoop install charm-gum
```


Or download it:

[Packages](https://github.com/charmbracelet/gum/releases) are available in Debian, RPM, and Alpine formats

[Binaries](https://github.com/charmbracelet/gum/releases) are available for Linux, macOS, Windows, FreeBSD, OpenBSD, and NetBSD

Or just install it with go:

go install github.com/charmbracelet/gum@latest
