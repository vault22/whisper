#!/bin/bash
# set -x 

export GUM_INPUT_CURSOR_FOREGROUND="212"
export GUM_INPUT_PROMPT_FOREGROUND="212"

# Checks if the optional FILENAME variable is set, and if not
# sets it to a random 8 letter string. FILENAME is then used
# as the OUTPUT_FILE variable.
checkOptionalFilename() {
    # Check if optional FILENAME variable is set
    if [ -n "$FILENAME" ]; then
      # If it's set, set output file to filename
      OUTPUT_FILE=$FILENAME
    else
      # Otherwise generate a random output filename
      RANDOM_STRING=$(tr -dc 'a-zA-Z0-9' < /dev/urandom | fold -w 8 | head -n 1)
      OUTPUT_FILE=$RANDOM_STRING.gpg
    fi
}

# Takes an input file (TEMP_FILE) and output file (OUTPUT_FILE)
# and encrypts the input file securely.
encryptFile() {
    export ENCRYPT_COMMAND
    # Set encryption command variable
    ENCRYPT_COMMAND=$(gpg --symmetric \
        --cipher-algo aes256 \
        --digest-algo sha512 \
        --cert-digest-algo sha512 \
        --compress-algo none -z 0 \
        --s2k-mode 3 \
        --s2k-digest-algo sha512 \
        --s2k-count 65011712 \
        --force-mdc \
        --pinentry-mode loopback \
        --armor \
        --no-symkey-cache --output "$OUTPUT_FILE"\
        "$TEMP_FILE")
}

# Greeting message when launching program
gum style \
	--foreground 212 --border-foreground 212 --border double \
	--align center --width 85 --margin "1 2" --padding "2 4" \
	'Whisper' 'Welcome to the encryptor!'

while true
do
  # Should the program run in encrypt or decrypt mode?
  MODE=$(gum choose --cursor.foreground 212 "New encrypted file" "Decrypt file" "Encrypt existing file" "Exit application")

  # What is the current mode (decrypt or encrypt)?
  case "$MODE" in

    "New encrypted file")
      # Create a new temporary file for writing to
      touch /tmp/tempWhisperFile.txt
      TEMP_FILE="/tmp/tempWhisperFile.txt"

      # Read and write text to encrypt later
      gum write --header.foreground 212 --header="Text to encrypt" --placeholder "Enter text to encrypt... (CTRL-D to finish)" > "$TEMP_FILE"

      # Read optional output filename
      FILENAME=$(gum input --prompt "Filename (optonal) >>>: " \
      --placeholder "output.gpg (if not set, output filename will be randomized)...")

      checkOptionalFilename "$FILENAME"


      # Confirm and encrypt the file 
      gum confirm "Proceed and encrypt file?" \
      && encryptFile \
      || echo 'skipping'

      # Remove temporary file
      shred --iterations=3 --zero --remove "$TEMP_FILE" 
      ;;

    "Decrypt file")
      # Select a file to encrypt
      INPUT_FILE=$(gum file .)
      # Set the password
      PWD=$(gum input --password --prompt "Enter decryption key password >>>: ")
      # Use the password as stdin (occasionally doesn't work)
      $PWD | gpg --passphrase-fd 0 -d "$INPUT_FILE"
      printf '\n\n'
      ;;

    "Encrypt existing file")
      INPUT_FILE=$(gum file .)
      # Read optional output filename
      FILENAME=$(gum input --prompt "Filename (optonal) >>>: " \
      --placeholder "output.gpg (if not set, output filename will be randomized)...")

      checkOptionalFilename "$FILENAME"

      # Set the input file as the temp file argument for the 
      # encryptFile function
      TEMP_FILE="$INPUT_FILE"

      # Confirm and encrypt the file 
      gum confirm "Proceed and encrypt file?" \
      && encryptFile \
      || echo 'skipping'
      ;;
  "Exit application")
    printf "Exiting application..."
    sleep 0.5s
    clear
    exit 0 
    ;;
  esac
done
